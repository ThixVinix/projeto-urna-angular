import { Component } from '@angular/core';
import { VotoService } from '../service/voto.service';

@Component({
  selector: 'app-voto',
  templateUrl: './voto.component.html',
  styleUrls: ['./voto.component.scss'],
})
export class VotoComponent {
  public rg: string = '';
  public numCandidato: number | undefined;

  constructor(private service: VotoService) {}

  /**
   * votar
   */
  public votar() {
    console.log(`Passando pelo "votar()" do componente "Voto"...`);
    console.log('RG digitado: ' + this.rg);
    console.log('Número do candidato digitado: ' + this.numCandidato);

    const informacoesVoto = {
      rg: this.rg,
      numCandidato: this.numCandidato,
    };

    this.service.adicionarVoto(informacoesVoto).subscribe(
      (resultado) => {
        console.log(resultado);
        this.limparCampos();
      },
      (error) => {
        console.error('Não foi possível adicionar o voto --- ' + error);
      }
    );
  }

  private limparCampos(): void {
    this.rg = '';
    this.numCandidato = undefined;
  }
}
