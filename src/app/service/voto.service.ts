import { HttpClient } from '@angular/common/Http';
import { Injectable } from '@angular/core';
import { Voto } from 'models/voto.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VotoService {
  private _listaVotos: Array<any> = [];
  private url = 'http://localhost:3000/votos';

  constructor(private httpClient: HttpClient) {}

  /**
   * Getter listaVotos
   * @return {Array<any> }
   */
  public get listaVotos(): Array<any> {
    return this._listaVotos;
  }

  /**
   * Setter listaVotos
   * @param {Array<any>} value
   */
  public set listaVotos(value: Array<any>) {
    this._listaVotos = value;
  }

  public getAllVotos(): Observable<Voto[]> {
    return this.httpClient.get<Voto[]>(this.url);
  }

  public adicionarVoto(voto: any): Observable<Voto> {
    console.log(`Adicionando voto no método "adicionarVoto" do "VotoService"...`);

    this.adicionarDataVoto(voto);
    //this.listaVotos.push(voto);

    return this.httpClient.post<Voto>(this.url, voto);
  }

  private adicionarDataVoto(voto: any) {
    voto.data = new Date();
  }
}
