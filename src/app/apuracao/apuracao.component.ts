import { Component, Input, OnInit } from '@angular/core';
import { Voto } from 'models/voto.models';
import { VotoService } from '../service/voto.service';

@Component({
  selector: 'app-apuracao',
  templateUrl: './apuracao.component.html',
  styleUrls: ['./apuracao.component.scss'],
})
export class ApuracaoComponent implements OnInit {
  //@Input() votos: Array<any> = [];
  public votos: Array<any> = [];

  constructor(private service: VotoService) {}

  ngOnInit(): void {
    // this.votos = this.service.listaVotos;
    console.log(`Passando pelo "ngOnInit" do componente "Apuracao"...`);

    this.service.getAllVotos().subscribe((votos: Voto[]) => {
      console.table(votos);
      this.votos = votos;
    });
  }
}
