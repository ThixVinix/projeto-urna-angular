export interface Voto {
  id?: number;
  rg: string;
  numCandidato: number;
  data?: Date;
}
